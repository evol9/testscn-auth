from marshmallow import Schema, fields, validate


class LoginSingUpRequestSchema(Schema):
    email = fields.Email(required=True)
    password = fields.Str(
        required=True,
        validate=[validate.Length(min=8, max=20)],
        load_only=True
    )


class MessageResponseSchema(Schema):
    message = fields.Str()


class AuthTokenResponseSchema(Schema):
    id = fields.Int()
    auth_token = fields.Str()


class UserRequestHeaderSchema(Schema):
    Authorization = fields.Str(description="Bearer auth_token", required=False)


class UserDataSchema(Schema):
    id = fields.Int()
    email = fields.Email()
    is_stuff = fields.Boolean()


class UsersDataSchema(Schema):
    data = fields.Nested(UserDataSchema, many=True)


class SuccessPublicKeyResponseSchema(Schema):
    algorithm = fields.Str()
    public_key = fields.Str()
