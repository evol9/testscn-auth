from aiohttp import web
from asyncpgsa import pg
from aiohttp_apispec import setup_aiohttp_apispec, validation_middleware

from settings import config
from routes import setup_routes
from middleware import error_middleware


async def init_pg(app):
    conf = app["config"]["postgres"]
    app.pg = pg
    await pg.init(
        host=conf["host"],
        port=conf["port"],
        database=conf["database"],
        user=conf["user"],
        password=conf["password"],
        min_size=conf["minsize"],
        max_size=conf["maxsize"]
    )


async def close_pg(app):
    await app.pg.pool.close()


def init_app():
    application = web.Application(middlewares=[error_middleware])
    setup_routes(application)

    setup_aiohttp_apispec(
        app=application,
        title="My Documentation",
        version="v1",
        url="/api/docs/swagger.json",
        swagger_path="/api/docs",
    )
    application.middlewares.append(validation_middleware)

    application["config"] = config
    application.on_startup.append(init_pg)
    application.on_cleanup.append(close_pg)
    return application


APPLICATION = init_app()
