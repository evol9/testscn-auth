from aiohttp import web


async def http401(message="You are not authorized!"):
    response_object = {
        "message": message
    }
    return web.json_response(response_object, status=401)
