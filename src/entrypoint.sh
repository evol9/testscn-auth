#!/bin/sh

echo Generating JWT keys...
openssl genrsa -out private.pem 2048
openssl rsa -in private.pem -pubout > public.pem

export JWT_PRIVATE_KEY=`cat private.pem`
export JWT_PUBLIC_KEY=`cat public.pem`


echo Checking connection...
while ! pg_isready -d ${DATABASE} -U ${DB_USER} -h ${DB_HOST} -p ${DB_PORT} > /dev/null 2> /dev/null; do
    echo "Connecting to ${DB_HOST} Failed"
    sleep 1
done
echo Connected

echo Running migrations...
alembic -c /alembic.ini upgrade head

echo Starting Gunicorn...
gunicorn app:APPLICATION \
        --bind 0.0.0.0:5000 \
        --worker-class aiohttp.worker.GunicornUVLoopWebWorker \
        --log-level=info \
        --access-logfile log/access.log \
        --error-logfile log/error.log
