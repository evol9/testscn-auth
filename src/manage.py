import logging
import argparse
import asyncio

import uvloop
from aiohttp import web

from app import init_app
from settings import ENV
from utils import UserExistsError, createsuperuser

if ENV == 'DEV':
    logging.basicConfig(level='INFO')


def main():
    parser = argparse.ArgumentParser(description="Available subcommands")
    subparsers = parser.add_subparsers(
        title="commands",
        description='valid commands',
        dest="command"
    )
    parser_run = subparsers.add_parser(
        "runserver",
        help="run aiohttp server"
    )
    parser_run.add_argument(
        "-p",
        type=int,
        default=8080,
        help="port (default: 8080)"
    )
    parser_run.add_argument(
        "-host",
        type=str,
        default="127.0.0.1",
        help="host (default: 127.0.0.1)"
    )

    parser_superuser = subparsers.add_parser(
        "createsuperuser",
        help="create super user"
    )
    required_arguments = parser_superuser.add_argument_group(
        "required named arguments"
    )
    required_arguments.add_argument(
        "-e",
        type=str,
        help="email",
        required=True
    )
    required_arguments.add_argument(
        "-p",
        type=str,
        help="password",
        required=True
    )

    args = parser.parse_args()
    if args.command == "runserver":
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
        app = init_app()
        web.run_app(app, host=args.host, port=args.p)

    elif args.command == "createsuperuser" and args.p and args.e:
        try:
            createsuperuser(args.e, args.p)
            print("Successfully created!")
        except UserExistsError:
            message = "User '%s' already exists.\n" % args.e
            parser.exit(1, message=message)

    else:
        parser.print_help()
        parser.exit(1)


if __name__ == "__main__":
    main()
