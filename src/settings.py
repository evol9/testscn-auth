import os
import pathlib
import re

import yaml


BASE_DIR = pathlib.Path(__file__).parent.parent
config_base_path = BASE_DIR / "config" / "base.yaml"

ENV = os.getenv("ENV", "DEV")

env_var_regexp = re.compile(r"^\$\{(.*)\}$")


def env_var_constructor(loader, node):
    raw_value = loader.construct_scalar(node)
    value = env_var_regexp.match(raw_value).group(1)
    default = None
    if len(value.split(":")) > 1:
        env_var, default = value.split(":")
    else:
        env_var = value
    return os.getenv(env_var, default)


yaml.add_implicit_resolver("!env_var", env_var_regexp, Loader=yaml.SafeLoader)
yaml.add_constructor("!env_var", env_var_constructor, Loader=yaml.SafeLoader)


def merge(user, default):
    if isinstance(user, dict) and isinstance(default, dict):
        for k, v in default.items():
            if k not in user:
                user[k] = v
            else:
                user[k] = merge(user[k], v)
    return user


def get_config(conf):
    path = BASE_DIR / "config" / conf
    with open(config_base_path) as f:
        base_config = yaml.safe_load(f)
        with open(path) as file:
            env_config = yaml.safe_load(file)
            merged = merge(env_config, base_config)
    return merged


configuration = dict()


config = get_config(f"{ENV.lower()}.yaml")
