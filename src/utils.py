import psycopg2

from settings import config
from security import generate_password_hash

conf = config["postgres"]

HOST = conf["host"]
PORT = conf["port"]
DB_NAME = conf["database"]
USER = conf["user"]
PASS = conf["password"]


class UserExistsError(Exception):
    pass


def createsuperuser(email, password):
    conn = psycopg2.connect(
        user=USER,
        host=HOST,
        port=PORT,
        password=PASS,
        dbname=DB_NAME
    )
    cur = conn.cursor()
    cur.execute(
        "SELECT EXISTS (SELECT * FROM \"user\" WHERE email=%s)",
        (email,)
    )
    user_exists = cur.fetchone()[0]
    if user_exists:
        cur.close()
        conn.close()
        raise UserExistsError()
    pwd_hash = generate_password_hash(password)
    row = "INSERT INTO \"user\"(email,password,is_stuff) VALUES(%s,%s,true)"
    cur.execute(row, (email, pwd_hash))
    conn.commit()
    cur.close()
    conn.close()
