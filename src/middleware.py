import logging

from aiohttp import web


def json_error(status_code: int, exception: Exception) -> web.Response:
    """
    Returns a Response from an exception.
    Used for error middleware.
    :param status_code:
    :param exception:
    :return:
    """
    response_object = {
            'error': exception.__class__.__name__,
            'detail': str(exception)
        }
    return web.json_response(response_object, status=status_code)


@web.middleware
async def error_middleware(request, handler):
    try:
        response = await handler(request)
        return response
    except web.HTTPException as ex:
        if ex.status == 404:
            return json_error(ex.status, ex)
        raise
    except Exception as e:
        logging.warning(
            'Request {} has failed with exception: {}'.format(
                request,
                repr(e)
            )
        )
        return json_error(500, e)
