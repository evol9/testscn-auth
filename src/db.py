from sqlalchemy import (
    MetaData, Table, Column,
    Integer, String, Boolean
)

meta = MetaData()

user = Table(
    "user", meta,

    Column("id", Integer, primary_key=True),
    Column("email", String(200), nullable=False, unique=True),
    Column("password", String(200), nullable=False),
    Column("is_stuff", Boolean, nullable=False, default=False)
)
