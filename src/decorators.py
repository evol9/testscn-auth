from functools import wraps
import logging

import jwt

from security import decode_auth_token
from errorhandlers import http401


def login_required(f):
    @wraps(f)
    async def wrapper(request):
        if "Authorization" not in request.headers:
            return await http401()
        request.user_id = None
        request.user_is_stuff = False
        try:
            auth_header = request.headers.get("Authorization")
            auth_token = auth_header.split(" ")[1]
            if not auth_token:
                return await http401(
                    message="Provide a valid auth token."
                )

            try:
                payload = decode_auth_token(auth_token)
            except jwt.ExpiredSignatureError:
                message = "Signature expired. Please log in again."
                logging.info("Signature expired.")
                return await http401(message=message)
            except jwt.InvalidTokenError:
                message = "Invalid token. Please log in again."
                logging.info("Invalid token.")
                return await http401(message=message)
            request.user_id = payload["id"]
            request.user_is_stuff = payload["is_stuff"]
            return await f(request)
        except IndexError:
            message = "Provide a valid Authorization token."
            return await http401(message=message)
    return wrapper


def stuff_required(f):
    @wraps(f)
    async def wrapper(request):
        if request.user_is_stuff:
            return await f(request)
        else:
            return await http401(
                message="You do not have permission."
            )
    return wrapper
