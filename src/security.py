import datetime
import os

import bcrypt
import jwt


JWT_PRIVATE_KEY = os.getenv("JWT_PRIVATE_KEY")
JWT_PUBLIC_KEY = os.getenv("JWT_PUBLIC_KEY")


def generate_password_hash(password):
    password_bin = password.encode("utf-8")
    hashed = bcrypt.hashpw(password_bin, bcrypt.gensalt())
    return hashed.decode("utf-8")


def check_password_hash(plain_password, password_hash):
    plain_password_bin = plain_password.encode("utf-8")
    password_hash_bin = password_hash.encode("utf-8")
    is_correct = bcrypt.checkpw(plain_password_bin, password_hash_bin)
    return is_correct


def encode_auth_token(user_id, is_stuff):
    payload = {
        "exp": datetime.datetime.utcnow() + datetime.timedelta(minutes=15),
        "iat": datetime.datetime.utcnow(),
        "sub": {
            "id": user_id,
            "is_stuff": is_stuff
        }
    }
    return jwt.encode(
        payload,
        JWT_PRIVATE_KEY,
        algorithm='RS256'
    )


def decode_auth_token(auth_token):
    payload = jwt.decode(auth_token, JWT_PUBLIC_KEY, algorithms="RS256")
    return payload["sub"]
