import logging

from aiohttp import web
from aiohttp_apispec import (
    docs,
    request_schema,
    response_schema,
    use_kwargs
)
from sqlalchemy.sql import exists
from asyncpgsa import pg

from decorators import login_required, stuff_required
from security import (
    generate_password_hash,
    check_password_hash,
    encode_auth_token,
    JWT_PUBLIC_KEY
)
import db
from schemas import (
    LoginSingUpRequestSchema,

    UserDataSchema,
    UsersDataSchema,
    MessageResponseSchema,

    AuthTokenResponseSchema,

    SuccessPublicKeyResponseSchema,

    UserRequestHeaderSchema
)
from errorhandlers import http401


@docs(
    tags=["auth"],
    summary="Sing up",
    description="Sing up new user",
    responses={
        422: {"description": "Validation error"},
    }
)
@request_schema(LoginSingUpRequestSchema(strict=True))
@response_schema(UserDataSchema(), 201)
@response_schema(MessageResponseSchema(), 202)
@response_schema(MessageResponseSchema(), 422)
async def sing_up(request):
    data = request["data"]
    email = data["email"]
    pwd = data["password"]
    # user_exists = await request.app.pg.fetchval(
    #     'SELECT EXISTS (SELECT * FROM "user" WHERE email=$1)',
    #     email
    # )
    user_exists = await pg.fetchval(
        exists().where(db.user.c.email == email).select()
    )

    if user_exists:
        response_object = {
            "message": "User already exists. Please Log in."
        }
        logging.info("User already exists. Sing up.")
        return web.json_response(response_object, status=422)
    hash_pwd = generate_password_hash(pwd)
    # execute SQL 'INSERT INTO "user"(email,password) VALUES(email, hashpwd)'
    # pylint: disable=no-value-for-parameter
    row = db.user.insert().values(email=email, password=hash_pwd)
    await pg.execute(row)

    user = await pg.fetchrow(
        db.user.select().where(
            db.user.c.email == email
        )
    )

    response_object = {k: user[k] for k in ["id", "email", "is_stuff"]}
    return web.json_response(response_object, status=201)


@docs(
    tags=["auth"],
    summary="Login",
    description="login user",
    responses={
        422: {"description": "Validation error"},
    }
)
@request_schema(LoginSingUpRequestSchema(strict=True))
@response_schema(MessageResponseSchema(), 401)
@response_schema(MessageResponseSchema(), 404)
@response_schema(AuthTokenResponseSchema(), 200)
async def login(request):
    data = request["data"]
    email = data["email"]
    pwd = data["password"]
    # execute sql "SELECT * FROM "user" WHERE email=email"
    user = await pg.fetchrow(
        db.user.select().where(
            db.user.c.email == email
        )
    )

    if not user:
        response_object = {
            "message": "User does not exist.",
        }
        logging.info("User does not exist. Login.")
        return web.json_response(response_object, status=404)

    if check_password_hash(pwd, user["password"]):
        auth_token = encode_auth_token(user["id"], user["is_stuff"])
        if auth_token:
            response_object = {
                "id": user["id"],
                "auth_token": auth_token.decode()
            }
            return web.json_response(response_object, status=200)
    else:
        response_object = {
            "message": "Password does not match."
        }
        logging.info("Password does not match.")
        return web.json_response(response_object, status=401)


@docs(
    tags=["key"],
    summary="Get key",
    description="Public key"
)
@response_schema(SuccessPublicKeyResponseSchema(), 200)
@response_schema(MessageResponseSchema(), 404)
async def public_key_endpoint(request):
    if JWT_PUBLIC_KEY:
        response_object = {
            "algorithm": "RS256",
            "public_key": JWT_PUBLIC_KEY
        }
        return web.json_response(response_object, status=200)
    message = "Public key not found."
    response_object = {
        "message": message,
    }
    logging.warning(message)
    return web.json_response(response_object, status=404)


# User views
@docs(
    tags=["users"],
    summary="Users list",
    description="get all users list"
)
@use_kwargs(UserRequestHeaderSchema(strict=True), location="headers")
@response_schema(UsersDataSchema(), 200)
@response_schema(MessageResponseSchema(), 401)
@login_required
@stuff_required
async def users(request):
    # executing of SQL SELECT * FROM "user"'
    users = await pg.fetch(db.user.select())
    response_object = []
    for user in users:
        response_object.append(
            {
                "id": user["id"],
                "email": user["email"],
                "is_stuff": user["is_stuff"]
            }
        )
    return web.json_response({"data": response_object}, status=200)


@docs(
    tags=["users"],
    summary="Get user",
    description="get user email id"
)
@use_kwargs(UserRequestHeaderSchema(strict=True), location="headers")
@response_schema(UserDataSchema(), 200)
@response_schema(MessageResponseSchema(), 401)
@login_required
async def user_detail(request):
    id = int(request.match_info.get('id', 0))
    if not id == request.user_id:
        message = "Invalid user id."
        return await http401(message=message)
    user = await pg.fetchrow(
        db.user.select().where(
            db.user.c.id == request.user_id
        )
    )
    # executing of SQL 'SELECT * FROM "user" WHERE id=request.user_id'
    response_object = {k: user[k] for k in ["id", "email", "is_stuff"]}
    return web.json_response(response_object, status=200)
