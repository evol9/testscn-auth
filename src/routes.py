from views import sing_up, login, public_key_endpoint, users, user_detail


def setup_routes(app):
    app.router.add_get('/api/v1/users', users)
    app.router.add_get('/api/v1/users/{id}', user_detail)

    app.router.add_get('/api/v1/public-key', public_key_endpoint)
    app.router.add_post('/api/v1/singup', sing_up)
    app.router.add_post('/api/v1/login', login)
