"""user add isstuff column

Revision ID: ba2fa934e131
Revises: d6e983ffeb8d
Create Date: 2019-07-30 11:32:48.011579

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ba2fa934e131'
down_revision = 'd6e983ffeb8d'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('user', sa.Column('is_stuff', sa.Boolean, default=False))
    query = 'UPDATE "user" SET is_stuff=false;'
    op.execute(query)
    op.alter_column('user', 'is_stuff', nullable=False)
    # query = 'UPDATE "app" SET featured=true WHERE app.id IN (SELECT app_id FROM FEATURED);'
    # op.execute(query)


def downgrade():
    op.drop_column('user', 'is_stuff')
