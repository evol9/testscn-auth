"""create user table

Revision ID: d6e983ffeb8d
Revises:
Create Date: 2019-07-18 11:47:26.508733

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd6e983ffeb8d'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'user',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('email', sa.String(200), nullable=False, unique=True),
        sa.Column('password', sa.String(200), nullable=False)
    )


def downgrade():
    op.drop_table('user')
