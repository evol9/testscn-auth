#!/bin/sh

echo Running pylint...
pylint --errors-only src/ && echo "success" || exit 1;

echo Running flake8...
flake8 src/ && echo "success" || exit 1;


echo Running tests...
pytest tests/test.py -x