import os
import sys

import pytest
import psycopg2
from asyncpgsa import pg
from alembic.config import Config
from alembic import command


base_dir = os.path.normpath(os.path.join(os.path.dirname(
            os.path.realpath(__file__)),
            "../src"
        ))

sys.path.append(base_dir)
from settings import config
from app import init_app
from db import meta


conf = config["postgres"]

HOST = conf["host"]
PORT = conf["port"]
DB_NAME = conf["database"]
USER = conf["user"]
PASS = conf["password"]


@pytest.yield_fixture(scope="session")
def db_init():

    conn = psycopg2.connect(user=USER, host=HOST, port=PORT, password=PASS, dbname="postgres")
    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cur = conn.cursor()
    cur.execute("DROP DATABASE IF EXISTS %s" % DB_NAME)
    cur.execute("CREATE DATABASE " + DB_NAME)
    cur.close()

    alembic_ini_path = os.path.join(
        os.path.normpath(os.path.join(os.path.dirname(
            os.path.realpath(__file__)),
            ".."
        )),
        "alembic.ini"
    )
    alembic_cfg = Config(alembic_ini_path)
    alembic_cfg.set_main_option("script_location", "alembic")
    command.upgrade(alembic_cfg, "head")

    yield
    cur = conn.cursor()
    cur.execute("DROP DATABASE %s" % DB_NAME)
    cur.close()
    conn.close()


@pytest.yield_fixture(scope="function", autouse=True)
async def truncation(cli):
    yield
    tables = ','.join("\"" + table.name + "\"" for table in reversed(meta.sorted_tables))
    await pg.execute("TRUNCATE " + tables)


@pytest.fixture
async def cli(loop, aiohttp_client, db_init):
    app = init_app()
    return await aiohttp_client(app)
