from utils import createsuperuser


async def test_unauthorized(cli):
    resp = await cli.get("/api/v1/users")
    assert resp.status == 401
    assert (await resp.json())["message"] == "You are not authorized!"


async def test_singup(cli):
    resp = await cli.post("/api/v1/singup", json={
            "email": "test@test.com",
            "password": "qawsedrf"
        }, headers={
        "Content-Type": "application/json"
        }
        )
    assert resp.status == 201
    assert resp.content_type == "application/json"
    assert (await resp.json())["email"] == "test@test.com"
    assert (await resp.json())["id"] is not None
    assert (await resp.json())["is_stuff"] is False


async def test_user_exists_sing_up(cli):
    resp = await cli.post("/api/v1/singup", json={
            "email": "test@test.com",
            "password": "qawsedrf"
    }, headers={
        "Content-Type": "application/json"
        }
    )
    assert resp.status == 201
    assert resp.content_type == "application/json"
    assert (await resp.json())["id"] is not None
    assert (await resp.json())["email"] == "test@test.com"
    assert (await resp.json())["is_stuff"] is False

    resp = await cli.post("/api/v1/singup", json={
            "email": "test@test.com",
            "password": "qawsedrf"
    }, headers={
        "Content-Type": "application/json"
        }
    )
    assert resp.status == 422
    assert resp.content_type == "application/json"
    assert (await resp.json())["message"] == "User already exists. Please Log in."


async def test_sing_up_short_password_length(cli):
    resp = await cli.post("/api/v1/singup", json={
        "email": "test@test.com",
        "password": "qawsedr"
    }, headers={
        "Content-Type": "application/json"
    }
                          )
    assert resp.status == 422
    assert resp.content_type == "application/json"
    assert (await resp.json())["password"] == ["Length must be between 8 and 20."]


async def test_sing_up_long_password_length_not_valid_email(cli):
    resp = await cli.post("/api/v1/singup", json={
        "email": "testtest.com",
        "password": "qawsedrfqawsedrfqawse"
    }, headers={
        "Content-Type": "application/json"
    }
                          )
    assert resp.status == 422
    assert resp.content_type == "application/json"
    assert (await resp.json())["password"] == ["Length must be between 8 and 20."]
    assert (await resp.json())["email"] == ["Not a valid email address."]


async def test_login_long_password_length(cli):
    resp = await cli.post("/api/v1/login", json={
        "email": "test@test.com",
        "password": "qawsedrfqawsedrfqawse"
    }, headers={
        "Content-Type": "application/json"
    }
                          )
    assert resp.status == 422
    assert resp.content_type == "application/json"
    assert (await resp.json())["password"] == ["Length must be between 8 and 20."]


async def test_login_short_password_length_not_valid_email(cli):
    resp = await cli.post("/api/v1/login", json={
        "email": "test@testcom",
        "password": "qa"
    }, headers={
        "Content-Type": "application/json"
    }
                          )
    assert resp.status == 422
    assert resp.content_type == "application/json"
    assert (await resp.json())["password"] == ["Length must be between 8 and 20."]
    assert (await resp.json())["email"] == ["Not a valid email address."]


async def test_registered_user_login(cli):
    resp = await cli.post("/api/v1/singup", json={
        "email": "test@test.com",
        "password": "qawsedrf"
    }, headers={
        "Content-Type": "application/json"
        }
    )
    assert resp.status == 201
    assert resp.content_type == "application/json"
    assert (await resp.json())["id"] is not None
    assert (await resp.json())["email"] == "test@test.com"
    assert (await resp.json())["is_stuff"] is False
    resp = await cli.post("/api/v1/login", json={
        "email": "test@test.com",
        "password": "qawsedrf"
    }, headers={
        "Content-Type": "application/json"
        }
    )
    assert resp.status == 200
    assert resp.content_type == "application/json"
    assert (await resp.json())["auth_token"] is not None
    assert (await resp.json())["id"] is not None


async def test_registered_user_login_password_does_not_match(cli):
    resp = await cli.post("/api/v1/singup", json={
        "email": "test@test.com",
        "password": "qawsedrf"
    }, headers={
        "Content-Type": "application/json"
        }
    )
    assert resp.status == 201
    resp = await cli.post("/api/v1/login", json={
        "email": "test@test.com",
        "password": "invalidPassword"
    }, headers={
        "Content-Type": "application/json"
        }
    )
    assert resp.status == 401
    assert resp.content_type == "application/json"
    assert (await resp.json())["message"] == "Password does not match."


async def test_non_registered_user_login(cli):
    resp = await cli.post("/api/v1/login", json={
        "email": "test@test.com",
        "password": "qawsedrf"
    }, headers={
        "Content-Type": "application/json"
        }
    )
    assert resp.status == 404
    assert resp.content_type == "application/json"
    assert (await resp.json())["message"] == "User does not exist."


async def test_index_page_with_no_stuff(cli):
    resp = await cli.post("/api/v1/singup", json={
        "email": "test@test.com",
        "password": "qawsedrf"
    })
    assert resp.status == 201
    assert resp.content_type == "application/json"
    assert (await resp.json())["email"] == "test@test.com"
    assert (await resp.json())["is_stuff"] is False
    assert (await resp.json())["id"] is not None
    resp = await cli.post("/api/v1/login", json={
        "email": "test@test.com",
        "password": "qawsedrf"
    }, headers={
        "Content-Type": "application/json"
        }
    )
    assert resp.status == 200
    assert resp.content_type == "application/json"
    assert (await resp.json())["auth_token"] is not None
    assert (await resp.json())["id"] is not None
    token = (await resp.json())["auth_token"]

    resp = await cli.get("/api/v1/users", headers={
        "Authorization": "Bearer " + token,
    })
    assert resp.status == 401
    assert (await resp.json())["message"] == "You do not have permission."


async def test_user_detail_data(cli):
    resp_register = await cli.post("/api/v1/singup", json={
            "email": "test@test.com",
            "password": "qawsedrf"
    }, headers={
        "Content-Type": "application/json"
        }
    )
    assert resp_register.status == 201
    assert resp_register.content_type == "application/json"
    assert (await resp_register.json())["email"] == "test@test.com"
    assert (await resp_register.json())["is_stuff"] is False
    assert (await resp_register.json())["id"] is not None

    resp_login = await cli.post("/api/v1/login", json={
        "email": "test@test.com",
        "password": "qawsedrf"
    }, headers={
        "Content-Type": "application/json"
    }
                          )
    assert resp_login.status == 200
    assert resp_login.content_type == "application/json"
    assert (await resp_login.json())["auth_token"] is not None
    assert (await resp_login.json())["id"] is not None
    id = (await resp_login.json())["id"]
    token = (await resp_login.json())["auth_token"]

    url = f"/api/v1/users/{id}"

    resp = await cli.get(url, headers={
        "Authorization": "Bearer " + token,
    })
    assert resp.status == 200
    assert resp.content_type == "application/json"
    assert (await resp.json())["id"] is not None
    assert (await resp.json())["email"] == "test@test.com"
    assert (await resp.json())["is_stuff"] is False


async def test_user_detail_not_authorized(cli):
    resp = await cli.get("/api/v1/users")
    assert resp.status == 401
    assert resp.content_type == "application/json"
    assert (await resp.json())["message"] == "You are not authorized!"


async def test_user_detail_invalid_token(cli):
    resp = await cli.get("/api/v1/users/1", headers={
        "Authorization": "Bearer " + "invalid token",
    })
    assert resp.status == 401
    assert resp.content_type == "application/json"
    assert (await resp.json())["message"] == "Invalid token. Please log in again."


async def test_public_key_endpoint(cli):
    resp = await cli.get("/api/v1/public-key")
    assert resp.status == 200
    assert resp.content_type == "application/json"
    assert (await resp.json())["public_key"] is not None


async def test_superuser_user_list(cli):
    createsuperuser("test@test.com", "qawsedrf")
    resp = await cli.post("/api/v1/login", json={
        "email": "test@test.com",
        "password": "qawsedrf"
    }, headers={
        "Content-Type": "application/json"
    }
                          )
    assert resp.status == 200
    assert resp.content_type == "application/json"
    assert (await resp.json())["auth_token"] is not None
    assert (await resp.json())["id"] is not None
    token = (await resp.json())["auth_token"]

    resp = await cli.get("/api/v1/users", headers={
        "Authorization": "Bearer " + token,
    })
    assert resp.status == 200
    assert (await resp.json())["data"] is not None
