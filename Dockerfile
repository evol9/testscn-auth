FROM python:alpine3.7

RUN pip install --upgrade pip

RUN apk add --no-cache gcc g++ linux-headers postgresql-dev libffi-dev openssl postgresql-client make

COPY config/requirements.txt /tmp/requirements.txt

RUN pip install -r /tmp/requirements.txt

COPY config /config

COPY alembic /alembic

COPY alembic.ini /alembic.ini

COPY src /src

WORKDIR /src

RUN chmod +x entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]